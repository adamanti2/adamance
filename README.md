# adamance

Adamanti's personal day planner (this is a personal-use project)

Only runs on MacOS due to OS-dependent features such as notifications but can be expanded

### Documentation for future me
```
adamance list      Lists all of today's tasks in tasks-today.yaml
adamance add       Schedules a new task for today
adamance cancel    Removes the task with a specific index from today's tasks
adamance start     Starts the task with a specific index
adamance stop      Cancels the currently active task
adamance current   Shows the currently active task
adamance combine   Combines two tasks with the same activity
adamance generate  Re-generates the task list for today

adamance activity add     Add an activity to activities.yaml
adamance activity list    List all activities in activities.yaml
adamance activity remove  Remove 1 or more activities with specified name(s)

adamance bg start  Starts adamance's background if not running
adamance bg stop   Stops adamance's background if it is running
adamance open      Open one of adamance's user files in the standard text editor. The default extension is yaml
```

For development the package needs to be installed in editable mode with `pip install -e .` from the root folder, as each subpackage uses `import adamance` imports

### TODO list
- Custom notification icon in rumps
- activity.defaults
- task.scheduledAt, task.before, task.after
- pydantic validation code can be made more elegant with mode='wrap' validators and model_validate() methods
