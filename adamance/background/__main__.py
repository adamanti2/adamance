import setproctitle
import asyncio
from adamance.background.tasks.locks import checkLocks
from adamance.background.tasks.watch import watch


def main():
    setproctitle.setproctitle("adamance-background")

    loop = asyncio.new_event_loop()

    task = loop.create_task(checkLocks())
    loop.create_task(watch())

    try:
        loop.run_until_complete(task)
    except asyncio.CancelledError:
        loop.stop()


if __name__ == "__main__":
    main()
