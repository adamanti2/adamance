from typing import Optional
from adamance.background._os._base import BaseOSEvents
from adamance.shared import activities_tasks

import rumps
from playsound import playsound
from multiprocessing import Process, Queue
from adamance.shared.consts import Sounds

class _QueueEvents:
    STOP_TASK = 0
    TICK = 1
    TICK_BREAK = 2
    TICK_GRACE = 3

class MacOSEvents(BaseOSEvents):
    DIAMOND = "♢"

    @classmethod
    def _startRumps(cls, queue: Queue):
        app = rumps.App(name="adamance", title=cls.DIAMOND)

        def _appTimer(sender):
            while not queue.empty():
                event = queue.get()
                if event[0] == _QueueEvents.STOP_TASK:
                    app.title = cls.DIAMOND
                elif event[0] == _QueueEvents.TICK or event[0] == _QueueEvents.TICK_BREAK or event[0] == _QueueEvents.TICK_GRACE:
                    seconds = event[1]
                    if seconds is None:
                        app.title = cls.DIAMOND
                    else:
                        hours = seconds // 3600
                        seconds %= 3600
                        minutes = seconds // 60
                        seconds %= 60

                        format = "{} " + cls.DIAMOND
                        suffix = cls.DIAMOND
                        if event[0] == _QueueEvents.TICK_BREAK:
                            format = "{} B" + cls.DIAMOND
                        elif event[0] == _QueueEvents.TICK_GRACE:
                            format = "({}) " + cls.DIAMOND

                        if hours > 0 or event[0] == _QueueEvents.TICK_GRACE:
                            timeStr = f"{hours}:{str(minutes).zfill(2)}"
                        else:
                            timeStr = f"{minutes}:{str(seconds).zfill(2)}"
                        app.title = format.format(timeStr)
        
        def _openTerminal(sender):
            pass

        buttonOpenTerminal = rumps.MenuItem(title="Open terminal", callback=_openTerminal)
        app.menu = [ buttonOpenTerminal ]

        timer = rumps.Timer(_appTimer, 0.25)
        timer.start()

        app.run()

    appProcess: Optional[Process] = None
    appQueue: Optional[Queue] = None
    @classmethod
    def startBackground(cls):
        cls.appQueue = Queue()
        cls.appProcess = Process(target=cls._startRumps, args=(cls.appQueue,))
        cls.appProcess.start()

    @classmethod
    def stopBackground(cls):
        if cls.appProcess:
            cls.appProcess.kill()
    
    @classmethod
    def tickTask(cls, task: activities_tasks.Task):
        if cls.appQueue and task.startedAt:
            if task.currentBreak:
                cls.appQueue.put((_QueueEvents.TICK_BREAK, task.breakSecondsLeft))
            else:
                cls.appQueue.put((_QueueEvents.TICK, task.secondsLeft))
    
    @classmethod
    def stopTask(cls, task: activities_tasks.Task):
        pass
    
    @classmethod
    def finishTask(cls, task: activities_tasks.Task):
        message = f"'{task.activity.name}' has been completed"
        if task.unlocks:
            message += f"; {task.unlocks.durationMin}m {task.unlocks.activity.name} unlocked"
        rumps.notification(
            title="Task complete",
            subtitle="",
            message=message,
            sound=False,
        )
        playsound(Sounds.NOTIF_HIGH, block=False)
    
    @classmethod
    def startTask(cls, task: activities_tasks.Task):
        pass
    
    @classmethod
    def startBreak(cls, taskBreak: activities_tasks.Break, task: activities_tasks.Task):
        message = f"Take a break from '{task.activity.name}' for {taskBreak.durationMin} minutes"
        rumps.notification(
            title="Break time",
            subtitle="",
            message=message,
            sound=False,
        )
        playsound(Sounds.NOTIF_UP, block=False)
    
    @classmethod
    def finishBreak(cls, taskBreak: activities_tasks.Break, task: activities_tasks.Task):
        message = f"Let's get back to the task ({task.activity.name})"
        rumps.notification(
            title="Break's over",
            subtitle="",
            message=message,
            sound=False,
        )
        playsound(Sounds.NOTIF_DOWN, block=False)
    
    @classmethod
    def tickGraceTime(cls, graceSec: int | None):
        if cls.appQueue:
            if graceSec is not None and graceSec < 0:
                graceSec = 0
            cls.appQueue.put((_QueueEvents.TICK_GRACE, graceSec))
    
    @classmethod
    def depleteGraceTime(cls):
        rumps.notification(
            title="Grace time expired",
            subtitle="",
            message="Adamance's grace time has expired. You may need to shorten some tasks.",
            sound=False,
        )
        playsound(Sounds.NOTIF_LOW, block=False)
