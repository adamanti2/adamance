from adamance.shared import activities_tasks

class BaseOSEvents:
    @classmethod
    def startBackground(cls):
        raise NotImplementedError()
    
    @classmethod
    def stopBackground(cls):
        raise NotImplementedError()
    
    @classmethod
    def tickTask(cls, task: activities_tasks.Task):
        raise NotImplementedError()
    
    @classmethod
    def stopTask(cls, task: activities_tasks.Task):
        raise NotImplementedError()
    
    @classmethod
    def finishTask(cls, task: activities_tasks.Task):
        raise NotImplementedError()
    
    @classmethod
    def startTask(cls, task: activities_tasks.Task):
        raise NotImplementedError()
    
    @classmethod
    def startBreak(cls, taskBreak: activities_tasks.Break, task: activities_tasks.Task):
        raise NotImplementedError()
    
    @classmethod
    def finishBreak(cls, taskBreak: activities_tasks.Break, task: activities_tasks.Task):
        raise NotImplementedError()
    
    @classmethod
    def tickGraceTime(cls, graceSec: int | None):
        raise NotImplementedError()
    
    @classmethod
    def depleteGraceTime(cls):
        raise NotImplementedError()
