import os
from asyncio import sleep
from adamance.background.logging import log
from pydantic import BaseModel
from adamance.shared.user_file._base import UserFileYaml
from adamance.shared.consts import ROOT_PATH
from typing import Any, Callable, Type, TYPE_CHECKING

async def watch_user_file(userFileType: Type[UserFileYaml], callback: Callable[[Any], Any], readonly: bool = True):
    lastMTime: float = 0
    while True:
        path = ROOT_PATH.parent / "user" / f"{userFileType.PATH}.yaml"

        if os.path.isfile(path):
            mtime = os.path.getmtime(path)
            if mtime != lastMTime:
                lastMTime = mtime
                try:
                    with userFileType(readonly=readonly) as userFile:
                        callback(userFile)
                except Exception as e:
                    log(f"Error while reading {userFileType.__name__}: {type(e)}: {e}")
        
        await sleep(1)
