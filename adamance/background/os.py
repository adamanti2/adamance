from adamance.background._os._base import BaseOSEvents
from adamance.shared.consts import CURRENT_OS, OSs

OSEvents = BaseOSEvents

if CURRENT_OS == OSs.MACOS:
    from adamance.background._os.mac import MacOSEvents
    OSEvents = MacOSEvents
