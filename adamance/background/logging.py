from adamance.shared.consts import BG_PATH

def log(*values):
    line = ' '.join(map(str, values))
    print(line)
    with open(BG_PATH / "bg.log", "a") as f:
        f.write(line + "\n")
