import os
import asyncio
from adamance.shared.consts import BG_PATH
from adamance.background.os import OSEvents

async def checkLocks():
    while True:
        checkStop()
        updateLock()
        await asyncio.sleep(1)

def checkStop():
    stopPath = BG_PATH / "stop.lock"
    if not os.path.isfile(stopPath):
        return
    os.remove(stopPath)
    for task in asyncio.all_tasks():
        task.cancel()
    OSEvents.stopBackground()
    raise asyncio.CancelledError()

def updateLock():
    lockPath = BG_PATH / "bg.lock"
    with open(lockPath, "w") as f:
        f.write("")
