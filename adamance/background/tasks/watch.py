import asyncio
from datetime import datetime, time, timedelta
from adamance.background.logging import log
from adamance.background.os import OSEvents
from adamance.background.watch_user_file import watch_user_file
from adamance.shared import activities_tasks
from adamance.shared.user_files import ConfigYaml, ActivitiesYaml, TasksTodayYaml, HistoryYaml
from adamance.shared.user_file import config, activities, tasks_today


class _Storages:
    config: config.Config
    activities: activities.Activities
    tasksToday: tasks_today.TasksToday

async def dayResetTask(resetTime: time):
    now = datetime.now()
    reset = datetime.combine(datetime.now(), resetTime)
    if now > reset:
        reset += timedelta(days=1)
    secUntilReset = (reset - now).seconds
    await asyncio.sleep(secUntilReset + 1)
    with TasksTodayYaml() as _:
        pass

def init():
    global storages

    currentDayResetTask: asyncio.Task | None = None
    def configListener(configYaml: config.Config):
        nonlocal currentDayResetTask
        if currentDayResetTask is None or _Storages.config.dayResetsAt != configYaml.dayResetsAt:
            if currentDayResetTask:
                currentDayResetTask.cancel()
            currentDayResetTask = asyncio.get_event_loop().create_task(dayResetTask(configYaml.dayResetsAt))
        _Storages.config = configYaml
    asyncio.get_event_loop().create_task(watch_user_file(ConfigYaml, configListener))

    def activitiesListener(activitiesYaml: activities.Activities):
        _Storages.activities = activitiesYaml
        with HistoryYaml() as historyYaml:
            for activity in activitiesYaml.allActivities:
                existingActivity = activities_tasks.find(lambda a: a.name == activity.name, historyYaml.activities)
                if not existingActivity:
                    historyYaml.activities.append(activity)
    asyncio.get_event_loop().create_task(watch_user_file(ActivitiesYaml, activitiesListener))

    def tasksTodayListener(tasksTodayYaml: tasks_today.TasksToday):
        if hasattr(_Storages, "tasksToday"):
            compareTaskState(tasksTodayYaml, _Storages.tasksToday)
        _Storages.tasksToday = tasksTodayYaml
    asyncio.get_event_loop().create_task(watch_user_file(TasksTodayYaml, tasksTodayListener, readonly=False))


async def watch():
    init()
    OSEvents.startBackground()
    lastBreak = None
    lastMinute = -1
    while True:
        await asyncio.sleep(1 - datetime.now().microsecond / 1000000)
        if _Storages.tasksToday.current:
            current = _Storages.tasksToday.current

            currentBreak = current.currentBreak if current else None
            if (lastBreak is None) != (currentBreak is None):
                if currentBreak:
                    startBreak(currentBreak, current)
                elif lastBreak:
                    finishBreak(lastBreak, current)
                else:
                    assert False
                lastBreak = currentBreak
            
            if current.finishedNow == 1:
                taskFinished(current)
            else:
                tickTask(current)
        
        elif _Storages.tasksToday.current is None:
            if datetime.now().minute != lastMinute:
                tickGraceTime()
        
        lastMinute = datetime.now().minute


def taskFinished(task: activities_tasks.Task):
    task.finished = 1
    OSEvents.finishTask(task)
    task.startedAt = None
    with TasksTodayYaml() as tasksTodayYaml:
        tasksTodayYaml.finished.append(task)
        if task.unlocks:
            tasksTodayYaml.unlocked.append(task.unlocks)
        tasksTodayYaml.current = None
    tickGraceTime()

def stopTask(task: activities_tasks.Task):
    OSEvents.stopTask(task)
    tickGraceTime()

def tickTask(task: activities_tasks.Task):
    OSEvents.tickTask(task)

def startBreak(taskBreak: activities_tasks.Break, task: activities_tasks.Task):
    OSEvents.startBreak(taskBreak, task)

def finishBreak(taskBreak: activities_tasks.Break, task: activities_tasks.Task):
    OSEvents.finishBreak(taskBreak, task)

graceTimeDepleted = False
def tickGraceTime():
    global graceTimeDepleted
    now = datetime.now()
    endTime = datetime.combine(datetime.now(), _Storages.config.dayEndsAt)
    resetTime = datetime.combine(datetime.now(), _Storages.config.dayResetsAt)
    if now > endTime or now < resetTime:
        OSEvents.tickGraceTime(None)
    else:
        graceTime = _Storages.tasksToday.getGraceTimeSeconds(_Storages.config.dayEndsAt)
        if graceTime is None:
            OSEvents.tickGraceTime(None)
        else:
            if graceTime <= 0 and not graceTimeDepleted:
                OSEvents.depleteGraceTime()
                graceTimeDepleted = True
            elif graceTime > 0:
                graceTimeDepleted = False
            OSEvents.tickGraceTime(graceTime)


def compareTaskState(new: tasks_today.TasksToday, prev: tasks_today.TasksToday):
    if new.current:
        if prev.current is None:
            OSEvents.startTask(new.current)
    
    if new.current is None and prev.current is not None:
        stopTask(prev.current)
    
    graceTime = new.getGraceTimeSeconds(_Storages.config.dayEndsAt)
    if graceTime != prev.getGraceTimeSeconds(_Storages.config.dayEndsAt):
        OSEvents.tickGraceTime(graceTime)
