from pathlib import Path
from enum import Enum, auto

__version__ = "0.0.0"
ROOT_PATH = Path(__file__).parent.parent
BG_PATH = ROOT_PATH / "background"
CLI_PATH = ROOT_PATH / "cli"

USER_PATH = ROOT_PATH.parent / "user"

ASSET_PATH = ROOT_PATH / "assets"
SOUND_PATH = ASSET_PATH / "sounds"
class Sounds:
    NOTIF_UP = SOUND_PATH / "notif-up.wav"
    NOTIF_DOWN = SOUND_PATH / "notif-down.wav"
    NOTIF_HIGH = SOUND_PATH / "notif-high.wav"
    NOTIF_LOW = SOUND_PATH / "notif-low.wav"

class OSs:
    MACOS = auto()

CURRENT_OS = OSs.MACOS # Currently only supported OS
