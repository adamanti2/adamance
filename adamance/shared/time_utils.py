from typing_extensions import Annotated
from pydantic.functional_validators import AfterValidator
from pydantic.functional_serializers import PlainSerializer
from datetime import time

def timeValidator(t: time) -> time:
    if t.hour == 0 and t.second > 0:
        return time(t.minute, t.second)
    return t.replace(tzinfo=None)

def timeSerializer(t: time) -> str:
    timeString = t.isoformat("minutes") if t.second == 0 else t.isoformat("seconds")
    return timeString

AdamanceTime = Annotated[time, AfterValidator(timeValidator), PlainSerializer(timeSerializer)]
