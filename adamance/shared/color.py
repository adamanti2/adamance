from ansi.color import fg, rgb, fx
from typing import Any, Tuple
from re import sub

def isValidColor(value: Any) -> bool:
    if not isinstance(value, str):
        return False
    return hasattr(fg, value) or isHexCode(value)

def isHexCode(string: str) -> bool:
    string = string.lstrip('#')
    if sub(r"[0-9a-fA-F]", "", string) != "":
        return False
    return len(string.lstrip('#')) in (3, 6)

def hexToRgb(hexCode: str) -> Tuple[int, int, int]:
    hexCode = hexCode.lstrip('#')
    if len(hexCode) == 3:
        rgb = tuple(hexCode)
        rgb = map(lambda n: n * 2, rgb)
    else:
        rgb = (hexCode[:2], hexCode[2:4], hexCode[4:])
    rgb = map(lambda n: int(n, 16), rgb)
    rgb = tuple(rgb)
    return (rgb[0], rgb[1], rgb[2])
