from adamance.shared.user_file._base import UserFileYaml
from adamance.shared.time_utils import AdamanceTime
from pydantic import BaseModel
from typing import List, Dict

from datetime import time


class Config(BaseModel):
    dayResetsAt: AdamanceTime = time(4, 30)
    dayEndsAt: AdamanceTime = time(22, 00)

class ConfigYaml(UserFileYaml):
    PATH = "config"
    STORAGE_MODEL = Config
    def __enter__(self) -> Config:
        return super().__enter__()
