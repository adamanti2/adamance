from adamance.shared.user_file._base import UserFileYaml
from pydantic import BaseModel, Field, model_validator
from typing import Any, List, Dict

from adamance.shared import activities_tasks

def _defaultActivities() -> List[activities_tasks.Activity]:
    return [
        activities_tasks.Activity(name="Project 1"),
        activities_tasks.Activity(name="Project 2"),
        activities_tasks.Activity(name="Chill"),
    ]

def _defaultChoiceActivities() -> List[activities_tasks.Activity]:
    choices = _defaultActivities()[:2]
    return [
        activities_tasks.ChoiceActivity(name="Projects", choices=choices)
    ]


class Activities(BaseModel):
    activities: List[activities_tasks.Activity] = Field(default_factory=_defaultActivities)
    choiceActivities: List[activities_tasks.ChoiceActivity] = Field(default_factory=_defaultChoiceActivities)

    @property
    def allActivities(self) -> List[activities_tasks.Activity]:
        return self.activities + self.choiceActivities

    @classmethod
    def _parseChoiceActivities(cls, choiceActivitiesRaw, choiceActivities, allActivities):
        notFound = []
        for activityRaw in choiceActivitiesRaw:
            choices = []
            allFound = True
            for choice in activityRaw["choices"]:
                for activity in allActivities:
                    if activity.name == choice:
                        break
                else:
                    allFound = False
                    break
                choices.append(activity)
            if not allFound:
                notFound.append(activityRaw)
                continue
            choiceActivities.append(activities_tasks.ChoiceActivity(
                name=activityRaw["name"],
                color=activityRaw.get("color", None),
                defaults=activityRaw.get("defaults", {}),
                choices=choices,
            ))
        return notFound

    @model_validator(mode="before")
    def validator(cls, data: Any) -> Any:
        if "activities" in data:
            activities = []
            for activity in data["activities"]:
                activities.append(activities_tasks.Activity(
                    name=activity["name"],
                    color=activity.get("color", None),
                    defaults=activity.get("defaults", {})
                ))
            data["activities"] = activities

            if "choiceActivities" in data:
                choiceActivities = []
                notFound = cls._parseChoiceActivities(data["choiceActivities"], choiceActivities, activities)
                notFound = cls._parseChoiceActivities(notFound, choiceActivities, activities + choiceActivities)
                data["choiceActivities"] = choiceActivities

        return data

class ActivitiesYaml(UserFileYaml):
    PATH = "activities"
    STORAGE_MODEL = Activities
    def __enter__(self) -> Activities:
        return super().__enter__()
