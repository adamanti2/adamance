from datetime import time
import yaml

yamlRepresenters = {}

def timeRepresenter(dumper: yaml.Dumper, data: time):
    timeString = data.isoformat("minutes") if data.second == 0 else data.isoformat("seconds")
    return dumper.represent_str(timeString)
yamlRepresenters[time] = timeRepresenter

def addRepresenters():
    for data_type, representer in yamlRepresenters.items():
        yaml.add_representer(data_type, representer)
