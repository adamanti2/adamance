from adamance.shared.user_file._base import UserFileYaml
from pydantic import BaseModel, model_serializer, model_validator
from typing import Any, List, Dict, Optional, Self, Type, TypeVar

from datetime import date
from adamance.shared import activities_tasks

_T = TypeVar("_T", activities_tasks.Activity, activities_tasks.ChoiceActivity)

class HistoryDay(BaseModel):
    finished: List[activities_tasks.Task] = []
    unfinished: List[activities_tasks.Task] = []

    @model_serializer(mode="wrap")
    def serializer(self, handler) -> Dict:
        dataDict = handler(self)

        for key in ("finished", "unfinished"):
            if key in dataDict and len(dataDict[key]) == 0:
                del dataDict[key]
        
        for task in dataDict.get("finished", []):
            if "finished" in task:
                del task["finished"]
        
        return dataDict
    
    @model_validator(mode="after")
    def validatorAfter(self):
        for task in self.finished:
            task.finished = 1
        return self

class History(BaseModel):
    activities: List[activities_tasks.Activity] = []
    days: Dict[date, HistoryDay] = {}

    @classmethod
    def _getActivity(cls, activityList: List[activities_tasks.Activity], name: str, activityType: Type[_T] = activities_tasks.Activity) -> _T:
        activity = activities_tasks.find(
            lambda activity: activity.name == name,
            activityList,
        )
        if activity is None or not isinstance(activity, activityType):
            activity = activityType(
                name=name
            )
        return activity

    @classmethod
    def _parseTask(cls, taskRaw: Dict, activityList: List[activities_tasks.Activity]) -> activities_tasks.Task:
        taskRaw["activity"] = cls._getActivity(activityList, taskRaw["activity"])
        if "chosenFrom" in taskRaw:
            taskRaw["chosenFrom"] = cls._getActivity(activityList, taskRaw["chosenFrom"], activities_tasks.ChoiceActivity)
        if "unlocks" in taskRaw:
            taskRaw["unlocks"] = cls._parseTask(taskRaw["unlocks"], activityList)
        return activities_tasks.Task.model_validate(taskRaw)

    @model_validator(mode="wrap")
    @classmethod
    def wrapValidator(cls, data, handler) -> Any:
        days: Dict[date, Dict] = data["days"]
        del data["days"]
        model: History = handler(data)
        
        for dayDate, dayRaw in days.items():
            finished: List[activities_tasks.Task] = []
            unfinished: List[activities_tasks.Task] = []

            if "finished" in dayRaw:
                for taskRaw in dayRaw["finished"]:
                    finished.append(cls._parseTask(taskRaw, model.activities))
            if "unfinished" in dayRaw:
                for taskRaw in dayRaw["unfinished"]:
                    unfinished.append(cls._parseTask(taskRaw, model.activities))
            
            model.days[dayDate] = HistoryDay(finished=finished, unfinished=unfinished)
        
        return model

class HistoryYaml(UserFileYaml):
    PATH = "history"
    STORAGE_MODEL = History
    def __enter__(self) -> History:
        return super().__enter__()
