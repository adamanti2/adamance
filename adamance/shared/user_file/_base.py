import yaml
from io import TextIOWrapper
from os import mkdir
from os.path import isfile, isdir
from pathlib import Path
from pydantic import BaseModel, ValidationError
from typing import Any, Type
from adamance.shared.consts import ROOT_PATH
from adamance.shared.user_file._representers import addRepresenters

addRepresenters()
openedFilepaths = set()

class UserFileError(Exception):
    pass

class UserFileYaml:
    PATH: str = ""
    STORAGE_MODEL: Type[BaseModel]

    readonly: bool
    _file: TextIOWrapper | None
    _prevContent: str

    _readonly: bool
    _preExisted: bool
    _storage: BaseModel
    _filePath: Path

    def __init__(self, readonly=False):
        cls = type(self)

        hasPath = cls.PATH != ""
        hasSchemaModel = hasattr(self, "STORAGE_MODEL")
        if not hasPath or not hasSchemaModel:
            if not hasPath and hasSchemaModel:
                raise NotImplementedError(f"UserFileYaml cannot be instantiated without PATH")
            elif hasPath and not hasSchemaModel:
                raise NotImplementedError(f"UserFileYaml cannot be instantiated without STORAGE_MODEL")
            elif not hasPath and not hasSchemaModel:
                raise NotImplementedError(f"UserFileYaml cannot be instantiated without PATH and STORAGE_MODEL")
        
        self._file = None
        self.readonly = readonly
        cls._filePath = ROOT_PATH.parent / "user" / f"{cls.PATH}.yaml"

    def __enter__(self) -> Any:
        cls = type(self)

        if cls._filePath in openedFilepaths:
            # Storage is already open by current process. Return existing storage
            if not self.readonly:
                cls._readonly = False
            return cls._storage
            
        else:
            # Storage is not currently open. Read and parse it from file
            if not isdir(ROOT_PATH.parent / "user"):
                mkdir(ROOT_PATH.parent / "user")
            
            cls._preExisted = True
            if not isfile(cls._filePath):
                cls._preExisted = False
            
            cls._readonly = self.readonly
            if cls._readonly and cls._preExisted:
                self._file = open(cls._filePath, "r")
            elif cls._preExisted:
                self._file = open(cls._filePath, "r+")
            else:
                self._file = open(cls._filePath, "w+")
            content = self._file.read()
            self._prevContent = content

            if content == "":
                yamlData = {}
            else:
                yamlData = yaml.load(content, yaml.Loader)
            
            if not isinstance(yamlData, dict):
                raise UserFileError(f"Failed to read {cls.PATH}.yaml: must be a mapping")
            
            try:
                cls._storage = self.STORAGE_MODEL.model_validate(yamlData)
            except ValidationError as e:
                raise UserFileError(f"Failed to parse {cls.PATH}.yaml into model {self.STORAGE_MODEL.__name__} due to the following error:\n{e}")
            openedFilepaths.add(cls._filePath)
            return cls._storage
    
    def __exit__(self, exc_type, exc_value, exc_traceback) -> bool:
        cls = type(self)

        if self._file is not None:
            try:
                if cls._filePath in openedFilepaths:
                    openedFilepaths.remove(cls._filePath)
                if not cls._readonly or not cls._preExisted:
                    dataDict = cls._storage.model_dump()
                    content = str(yaml.dump(dataDict, default_flow_style=False, sort_keys=False))
                    if self._prevContent != content:
                        self._file.seek(0)
                        self._file.truncate()
                        self._file.write(content)
                self._file.close()
            except Exception as e:
                print("-------")
                print(f"Error while saving user file {cls.PATH}.yaml!")
                print(e)
                try:
                    print(f"Storage data: {cls._storage}")
                except:
                    print("Unable to print storage!")
                print("-------")
        
        if exc_type is not None:
            return False
        return True
