from adamance.shared.generate_tasks import runTaskGenerator
from adamance.shared.user_file._base import UserFileYaml, UserFileError
from adamance.shared.user_file.history import HistoryYaml, HistoryDay
from adamance.shared.logging import log
from pydantic import BaseModel, Field, model_validator, model_serializer
from datetime import datetime, timedelta, time
from typing import Any, List, Optional, Type, TypeVar

from adamance.shared import activities_tasks
from adamance.shared.user_files import ActivitiesYaml, ConfigYaml

T = TypeVar("T", activities_tasks.Activity, activities_tasks.ChoiceActivity)

class TasksToday(BaseModel):
    @staticmethod
    def getToday():
        with ConfigYaml() as configYaml:
            now = datetime.now()
            day = now.day
            if now.time() < configYaml.dayResetsAt:
                day -= 1
            return day
        assert False
    
    date: int = Field(default_factory=getToday)
    current: Optional[activities_tasks.Task] = None
    tasks: List[activities_tasks.Task] = []
    unlocked: List[activities_tasks.Task] = []
    finished: List[activities_tasks.Task] = []

    @property
    def startableTasks(self) -> List[activities_tasks.Task]:
        return self.tasks + self.unlocked

    @property
    def allTasks(self) -> List[activities_tasks.Task]:
        tasks = self.startableTasks + self.finished
        if self.current:
            tasks.append(self.current)
        return tasks
    
    def removeTask(self, task: activities_tasks.Task) -> bool:
        if task in self.tasks:
            self.tasks.remove(task)
        elif task in self.unlocked:
            self.unlocked.remove(task)
        elif task in self.finished:
            self.finished.remove(task)
        else:
            return False
        return True
    
    def getGraceTimeSeconds(self, dayEndsAt: time) -> int | None:
        startableTasks = self.startableTasks
        
        now = datetime.now()
        dayEnd = datetime.combine(datetime.now(), dayEndsAt)
        if now > dayEnd:
            dayEnd += timedelta(days=1)
        graceTime = (dayEnd - now).seconds
        for task in startableTasks:
            graceTime -= task.secondsLeft
            if task.unlocks:
                graceTime -= task.unlocks.secondsLeft
        if self.current:
            graceTime -= self.current.secondsLeft
        return graceTime

    @classmethod
    def _getActivity(cls, activityList: List[activities_tasks.Activity], name: str, activityType: Type[T] = activities_tasks.Activity) -> T:
        activity = activities_tasks.find(
            lambda activity: activity.name == name,
            activityList,
        )
        if activity is None or not isinstance(activity, activityType):
            activity = activityType(
                name=name
            )
        return activity
    
    @classmethod
    def parseTask(cls, taskDict: dict, activityList: List[activities_tasks.Activity]) -> activities_tasks.Task:
        rawActivity: str = taskDict["activity"]
        activity = cls._getActivity(activityList, rawActivity)
        
        rawChosenFrom: str | None = taskDict.get("chosenFrom", None)
        chosenFrom = None if rawChosenFrom is None else cls._getActivity(activityList, rawChosenFrom, activityType=activities_tasks.ChoiceActivity)
        
        unlocks = None
        rawUnlocks: dict | None = taskDict.get("unlocks", None)
        if rawUnlocks:
            unlocks = cls.parseTask(rawUnlocks, activityList)

        task = activities_tasks.Task(
            activity=activity,
            chosenFrom=chosenFrom,
            durationMin=taskDict["durationMin"],

            scheduledAt=taskDict.get("scheduledAt", None),
            before=taskDict.get("before", None),
            after=taskDict.get("after", None),
            breaks=taskDict.get("breaks", []),
            
            unlocks=unlocks,
            finished=max(0, min(1, taskDict.get("finished", 0))),
        )
        return task
    
    def regenerateTasks(self, force: bool):
        prevDay = self.date
        prevFinished = self.finished
        prevUnfinished = self.tasks + self.unlocked
        if self.current:
            prevUnfinished.append(self.current)
        
        try:
            self.tasks = runTaskGenerator()
        except UserFileError as e:
            log(f"Failed to generate tasks from user/tasks.py: {e}")
            if not force:
                return
            self.tasks = []
        
        self.finished = []
        self.unlocked = []
        self.current = None
        self.date = self.getToday()

        if len(prevFinished + prevUnfinished) > 0:
            with HistoryYaml() as historyYaml:
                prevDateNow = datetime.now().replace(day=prevDay)
                prevDateLast = prevDateNow.replace(month=prevDateNow.month - 1)
                now = datetime.now()
                if now - prevDateNow < now - prevDateLast:
                    prevDate = prevDateNow
                else:
                    prevDate = prevDateLast
                historyYaml.days[prevDate.date()] = HistoryDay(
                    finished=prevFinished,
                    unfinished=prevUnfinished,
                )

    @model_validator(mode="before")
    def validatorBefore(cls, data: Any) -> Any:
        allTasks = data.get("tasks", []) + data.get("unlocked", []) + data.get("finished", [])
        if "current" in data and data["current"]:
            allTasks = [data["current"]] + allTasks
        
        tasks = []
        unlocked = []
        finished = []
        if len(allTasks) > 0:
            for task in data.get("finished", []):
                task["finished"] = 1
            with ActivitiesYaml() as activitiesYaml:
                allActivities = activitiesYaml.activities + activitiesYaml.choiceActivities
                for task in allTasks:
                    newTask = cls.parseTask(task, allActivities)
                    if "current" in data and task is data["current"]:
                        newTask.startedAt = activities_tasks.parseTime(task["startedAt"]) if "startedAt" in task else datetime.now().time()
                        if task.get("unlocked", None):
                            newTask.unlocked = True
                        data["current"] = newTask
                    else:
                        if newTask.finished == 1:
                            finished.append(newTask)
                        elif task in data["unlocked"]:
                            newTask.unlocked = True
                            unlocked.append(newTask)
                        else:
                            tasks.append(newTask)
        
        data["tasks"] = tasks
        data["unlocked"] = unlocked
        data["finished"] = finished

        return data
    
    @model_validator(mode="after")
    def validatorAfter(self):
        today = self.getToday()
        if self.date != today:
            self.regenerateTasks(force=True)
        return self
    
    @model_serializer(mode="wrap")
    def serializer(self, handler) -> dict:
        dataDict = handler(self)
        for taskRaw in dataDict.get("finished", []):
            del taskRaw["finished"]
        return dataDict

class TasksTodayYaml(UserFileYaml):
    PATH = "tasks-today"
    STORAGE_MODEL = TasksToday
    def __enter__(self) -> TasksToday:
        return super().__enter__()
