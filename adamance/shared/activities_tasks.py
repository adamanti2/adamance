from ansi.color import fg, rgb, fx
from pydantic import BaseModel, field_validator, model_serializer
from datetime import datetime, time
from math import floor
from typing import Any, Callable, Dict, Iterable, List, Optional, TypeVar
from adamance.shared.logging import log
from adamance.shared.color import isHexCode, hexToRgb, isValidColor
from adamance.shared.time_utils import AdamanceTime


T = TypeVar("T")
def find(predicate: Callable[[T], bool], iterable: Iterable[T]) -> Optional[T]:
    for item in iterable:
        if predicate(item):
            return item
    return None

def findI(predicate: Callable[[T], bool], iterable: Iterable[T]) -> Optional[int]:
    for i, item in enumerate(iterable):
        if predicate(item):
            return i
    return None

def parseTime(timeStr: str) -> time:
    unitsStr = timeStr.split(":")
    units = []
    for i in range(3):
        units.append(int(unitsStr[i]) if len(unitsStr) > i else 0)
    return time(*units)

class Activity(BaseModel):
    name: str
    color: Optional[str] = None
    defaults: dict = {}

    @field_validator("color")
    def colorValidator(cls, value: Any) -> str:
        if isValidColor(value) or value is None:
            return value
        suffix = " (<- invalid color!)"
        valueStr = str(value)
        if valueStr[-len(suffix):] != suffix:
            return valueStr + suffix
        return value

    @property
    def colorAnsi(self) -> str:
        if self.color is None:
            return ""
        elif hasattr(fg, self.color):
            return getattr(fg, self.color)
        elif isHexCode(self.color):
            return rgb.rgb256(*hexToRgb(self.color))
        else:
            return ""
    
    @property
    def endAnsi(self) -> str:
        if self.colorAnsi:
            return str(fx.reset)
        return ""

    @model_serializer(mode="wrap")
    def serializer(self, handler) -> Dict[str, Any]:
        dataDict = handler(self)
        if not self.color:
            del dataDict["color"]
        if not self.defaults:
            del dataDict["defaults"]
        return dataDict


class ChoiceActivity(Activity):
    choices: List[Activity] = []

    def choose(self, activity: Activity) -> Activity:
        if activity not in self.choices:
            raise TypeError("Activity not in choice list")
        activity.chosenFrom = self
        return activity
    
    def dict(self, *args, **kwargs):
        dataDict = super().dict(*args, **kwargs)
        dataDict["choices"] = [*map(lambda activity: activity.name, dataDict["choices"])]
        return dataDict

    @model_serializer(mode="wrap")
    def serializer(self, handler) -> Dict[str, Any]:
        dataDict = super().serializer(handler)
        dataDict["choices"] = []
        for choice in self.choices:
            dataDict["choices"].append(choice.name)
        return dataDict


class Break(BaseModel):
    timestampMin: int
    durationMin: int


class Task(BaseModel):
    activity: Activity
    chosenFrom: Optional[ChoiceActivity] = None
    durationMin: int

    scheduledAt: Optional[AdamanceTime] = None
    before: Optional[AdamanceTime] = None
    after: Optional[AdamanceTime] = None
    breaks: List[Break] = []
    
    unlocks: Optional["Task"] = None
    finished: float = 0

    startedAt: Optional[AdamanceTime] = None
    unlocked: Optional[bool] = None

    @property
    def finishedNow(self) -> float:
        finished = self.finished
        if self.startedAt:
            timePassed = datetime.now() - datetime.combine(datetime.now(), self.startedAt)
            sec = timePassed.seconds
            if timePassed.days < 0:
                sec = 60 * 60 * 24 - sec
            frac = sec / 60 / self.durationMin
            finished += frac
            finished = max(0, min(1, finished))
        return finished
    
    @property
    def secondsLeft(self) -> int:
        minutes = self.durationMin * (1 - self.finishedNow)
        seconds = round(minutes * 60)
        return seconds
    
    @property
    def currentBreak(self) -> Break | None:
        minutesPassed = self.durationMin - self.secondsLeft // 60
        for taskBreak in self.breaks:
            if taskBreak.timestampMin < minutesPassed and taskBreak.timestampMin + taskBreak.durationMin >= minutesPassed:
                return taskBreak
        return None
    
    @property
    def breakSecondsLeft(self) -> int | None:
        currentBreak = self.currentBreak
        if not currentBreak:
            return None
        secondsPassed = self.durationMin * 60 - self.secondsLeft
        breakSecondsPassed = secondsPassed - currentBreak.timestampMin * 60
        breakSecondsLeft = currentBreak.durationMin * 60 - breakSecondsPassed
        return breakSecondsLeft

    @field_validator('finished')
    def finishedValidator(cls, finished: float) -> float:
        return max(0, min(1, finished))

    def textShort(self, brackets: bool = False) -> str:
        text = ""

        minutes = self.durationMin
        if minutes > 60:
            hours = minutes // 60
            text += f"{hours}h "
            minutes = minutes % 60
        text += f"{minutes}m "

        text += f"'{self.activity.name}'"

        if brackets:
            text = f"{self.activity.colorAnsi}[{text}]{self.activity.endAnsi}"

        return text

    def text(self, brackets: bool = False) -> str:
        text = self.textShort()
        if self.chosenFrom:
            text += f" ('{self.chosenFrom.colorAnsi}{self.chosenFrom.name}{self.activity.colorAnsi or self.chosenFrom.endAnsi}')"

        infoLeft = []
        if self.scheduledAt:
            infoLeft.append(f"at {self.scheduledAt.isoformat("minutes")}")
        if self.before:
            infoLeft.append(f"before {self.before.isoformat("minutes")}")
        if self.after:
            infoLeft.append(f"after {self.after.isoformat("minutes")}")
        if self.breaks:
            infoLeft.append(f"{len(self.breaks)} breaks")
        if len(infoLeft) > 0:
            text += f" ({', '.join(infoLeft)})"

        infoRight = []
        if self.unlocks:
            infoRight.append(self.unlocks.textShort(brackets=True))
        if round(self.finishedNow * 100) > 0:
            percentage = round(self.finishedNow * 100)
            infoRight.append(str(percentage) + "%")
            if self.finishedNow < 1:
                frac = 1 - self.finishedNow
                hours = floor(frac * self.durationMin / 60)
                mins = floor(frac * self.durationMin % 60)
                secs = floor(frac * self.durationMin * 60 % 60)
                if hours > 0:
                    infoRight.append(f"(-{hours}:{str(mins).zfill(2)}:{str(secs).zfill(2)})")
                else:
                    infoRight.append(f"(-{mins}:{str(secs).zfill(2)})")
        if len(infoRight) > 0:
            text += f" ╎ {' '.join(infoRight)}"

        if brackets:
            text = f"{self.activity.colorAnsi}[ {text} ]{self.activity.endAnsi}"

        return text

    @model_serializer(mode="wrap")
    def serializer(self, handler) -> Dict[str, Any]:
        dataDict = handler(self)

        if isinstance(self.activity, Activity):
            dataDict["activity"] = self.activity.name
        
        if self.chosenFrom:
            dataDict["chosenFrom"] = self.chosenFrom.name
        
        if self.finished > 0:
            digits = 10000
            dataDict["finished"] = round(dataDict["finished"] * digits) / digits
        
        noneFields = tuple(filter(lambda pair: pair[1] is None, dataDict.items()))
        for key, value in noneFields:
            del dataDict[key]
        if self.unlocked == False:
            del dataDict["unlocked"]
        if self.finished == 0:
            del dataDict["finished"]
        if len(self.breaks) == 0:
            del dataDict["breaks"]
        
        return dataDict