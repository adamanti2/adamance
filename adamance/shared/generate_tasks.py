import importlib.util
from adamance.shared import activities_tasks
from adamance.shared.consts import ROOT_PATH
from adamance.shared.user_files import ActivitiesYaml, UserFileError
from typing import List

def runTaskGenerator() -> List[activities_tasks.Task]:
    spec = importlib.util.spec_from_file_location("tasks", ROOT_PATH.parent / "user" / "tasks.py")
    if not spec or not spec.loader:
        raise UserFileError("user/tasks.py file not found")
    module = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(module)
    if not hasattr(module, "__tasks__"):
        raise UserFileError("user/tasks.py has no __tasks__ function")
    with ActivitiesYaml() as activitiesYaml:
        try:
            tasks = module.__tasks__(activitiesYaml.allActivities)
        except TypeError:
            raise UserFileError("user/tasks.py __tasks__ function's arguments are incorrect (must be 1 positional argument that accepts the list of activities)")
        if not isinstance(tasks, list):
            raise UserFileError("user/tasks.py __tasks__ function did not return a list")
        for task in tasks:
            if not isinstance(task, activities_tasks.Task):
                raise UserFileError(f"user/tasks.py __tasks__ list contains a {type(task)} instead of tasks")
        return tasks
    raise UserFileError("Unable to load activities to generate tasks")
