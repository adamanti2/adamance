import click
import os
import subprocess
from sys import platform
from adamance.cli.cli_core import cli, AdamanceGroup, cliError, disableBgCheck
from adamance.cli.bg_interface import isBgRunning, stopBg, startBg
from adamance.shared.consts import USER_PATH

@cli.command()
@click.argument("filename")
def open(filename: str):
    """Open one of adamance's user files in the standard text editor. The default extension is yaml"""
    if "." not in filename:
        filename = filename + ".yaml"
    path = USER_PATH / filename

    if platform == "darwin":
        subprocess.call(('open', path))
    elif platform == "windows":
        os.startfile(path) # type: ignore
    else:
        subprocess.call(('xdg-open', path))
