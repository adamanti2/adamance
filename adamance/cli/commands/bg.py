import click
from adamance.cli.cli_core import cli, AdamanceGroup, cliError, disableBgCheck
from adamance.cli.bg_interface import isBgRunning, stopBg, startBg

@cli.group(cls=AdamanceGroup)
def bg():
    """Start/stop adamance's background process"""
    pass

@bg.command()
def start():
    """Starts adamance's background if not running"""
    disableBgCheck()
    if not isBgRunning():
        startBg()
        click.secho("started adamance!")
    else:
        cliError("adamance's background is already running")

@bg.command()
def stop():
    """Stops adamance's background if it is running"""
    disableBgCheck()
    if isBgRunning():
        stopBg()
        click.secho("stopped adamance's background.")
    else:
        cliError("adamance's background is not currently running")
