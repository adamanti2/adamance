from adamance.shared.color import isValidColor
import click
from ast import literal_eval
from adamance.cli.cli_core import cli, AdamanceGroup, cliError
from adamance.cli import ui
from adamance.shared import activities_tasks
from adamance.shared.user_files import ActivitiesYaml
from typing import List, Optional

@cli.group(cls=AdamanceGroup)
def activity():
    """Manage activities in activities.yaml"""
    pass

def representActivity(activity: activities_tasks.Activity) -> str:
    string = f"{activity.colorAnsi}{activity.name}{activity.endAnsi}"
    details = []
    if activity.color is not None:
        details.append(f"{activity.color}")
    if activity.defaults:
        details.append(f"{len(activity.defaults.keys())} task defaults")
    if len(details) > 0:
        string += f" ({', '.join(details)})"
    return string

@activity.command()
def list():
    """List all activities in activities.yaml"""
    with ActivitiesYaml() as activitiesYaml:
        if len(activitiesYaml.activities) == 0:
            click.secho(f"No activities saved")
        else:
            click.secho(f"List of activities ({len(activitiesYaml.activities)}):")
            for activity in activitiesYaml.activities:
                click.secho(" - " + representActivity(activity))
            click.secho()
        
        if len(activitiesYaml.activities) == 0:
            click.secho(f"No choice activities saved")
        else:
            click.secho(f"List of choice activities ({len(activitiesYaml.choiceActivities)}):")
            for activity in activitiesYaml.choiceActivities:
                click.secho(" - " + representActivity(activity) + ":")
                for choice in activity.choices:
                    click.secho(f"   - {choice.colorAnsi}{choice.name}{choice.endAnsi}")
            click.secho()


@activity.command()
@click.argument("names", nargs=-1)
def remove(names: List[str]):
    """Remove 1 or more activities with specified name(s)"""
    if len(names) == 0:
        namesStr = click.prompt("Enter activity name(s) to remove")
        names = namesStr.split(" ")
    
    with ActivitiesYaml() as activitiesYaml:
        for name in names:
            found = False
            for activityList in (activitiesYaml.activities, activitiesYaml.choiceActivities):
                i = activities_tasks.findI(lambda activity: activity.name == name, activityList)
                if i is not None:
                    found = True
            if not found:
                cliError(f"Unknown activity {name}")
                return
        
        removedCount = 0
        for name in names:
            for activityList in (activitiesYaml.activities, activitiesYaml.choiceActivities):
                i = activities_tasks.findI(lambda activity: activity.name == name, activityList)
                if i is not None:
                    removedCount += 1
                    activityList.pop(i)
        
        if removedCount == 1:
            click.secho(f"Removed activity {names[0]}.")
        else:
            click.secho(f"Removed {removedCount} activities.")


@activity.command()
@click.argument("name")
@click.option("-c", "--color", "colorStr", default="", help="The activity's color")
# @click.option("-d", "--defaults", "defaultsStr", default="", help="A dict of default task properties when creating a task with this activity") # TODO implement this
@click.option("-ch", "--choice", "isChoice", is_flag=True, help="Whether this is a choice activity (may specify --choices instead)")
@click.option("-cs", "--choices", "choicesStr", default=None, help="List of choices for this choice activity (may simply use --choice instead)")
def add(name: str, colorStr: str, defaultsStr: str, isChoice: bool, choicesStr: Optional[str]):
    """Add an activity to activities.yaml"""
    if name == "":
        name = click.prompt("Enter activity name")
    
    with ActivitiesYaml() as activitiesYaml:
        for activity in activitiesYaml.activities + activitiesYaml.choiceActivities:
            if activity.name == name:
                cliError(f"Activity {name} already exists!")
                return
        
        color = None
        if colorStr != "":
            if not isValidColor(colorStr):
                cliError(f"Invalid color '{colorStr}'!")
                return
            color = colorStr
        
        defaultsDict = {}
        if defaultsStr != "":
            try:
                defaultsDict = literal_eval(defaultsStr)
                if type(defaultsDict) is not dict:
                    raise ValueError()
            except (ValueError, SyntaxError):
                cliError(f"Invalid dict passed into 'defaults' option ({defaultsStr})")
                return
        
        if not (isChoice or choicesStr):
            newActivity = activities_tasks.Activity(
                name=name,
                color=color,
                defaults=defaultsDict,
            )
            activitiesYaml.activities.append(newActivity)
            click.secho("Added activity " + representActivity(newActivity))
        else:
            choices = []

            choicesStrings = []
            if choicesStr:
                choicesStr.split(" ")
            else:
                while True:
                    newChoiceName = click.prompt("Enter activity or activities to add to this choice activity (empty when done)", default="", show_default=False)
                    if newChoiceName == "":
                        break
                    choicesStrings += newChoiceName.split(" ")
            
            for choiceName in choicesStrings:
                choice = activities_tasks.find(lambda activity: activity.name == name, activitiesYaml.activities + activitiesYaml.choiceActivities)
                if choice:
                    choices.append(choice)
                else:
                    cliError(f"Unknown activity {choiceName}")
                    return
            
            newActivity = activities_tasks.ChoiceActivity(
                name=name,
                color=color,
                defaults=defaultsDict,
                choices=choices,
            )
            activitiesYaml.choiceActivities.append(newActivity)
            click.secho("Added choice activity " + representActivity(newActivity) + ":")
            for choice in newActivity.choices:
                click.secho(f" - {choice.colorAnsi}{choice.name}{choice.endAnsi}")
