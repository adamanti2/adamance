import click
from datetime import datetime
from adamance.cli import ui
from adamance.cli.cli_core import cli, cliError
from adamance.shared import activities_tasks
from adamance.shared.generate_tasks import runTaskGenerator
from adamance.shared.logging import log
from adamance.shared.user_files import ActivitiesYaml, TasksTodayYaml, ConfigYaml, UserFileError
from typing import List, Optional, Tuple

@cli.command()
def current():
    """Shows the currently active task"""
    with TasksTodayYaml() as tasksTodayYaml:
        if tasksTodayYaml.current:
            click.secho("Current task:")
            click.secho(" + " + tasksTodayYaml.current.text(brackets=True))
        else:
            click.secho("No task currently active. Use 'adamance list' or 'adamance start [task index]'")


@cli.command()
def list():
    """Lists all of today's tasks in tasks-today.yaml"""
    with TasksTodayYaml() as tasksTodayYaml:
        if tasksTodayYaml.current:
            click.secho("Current task:")
            click.secho(" + " + tasksTodayYaml.current.text(brackets=True))
            click.secho()

        if len(tasksTodayYaml.startableTasks) == 0:
            if tasksTodayYaml.current is not None:
                click.secho(f"No other tasks left for today")
            elif len(tasksTodayYaml.finished) == 0:
                click.secho(f"No tasks assigned for today")
            else:
                click.secho(f"No tasks left for today")
            click.secho()
        else:
            taskI = 0
            if len(tasksTodayYaml.tasks) > 0:
                click.secho(f"Today's tasks:")
                for task in tasksTodayYaml.tasks:
                    click.secho(f" ({taskI}) " + task.text(brackets=True))
                    taskI += 1
                click.secho()
            
            if len(tasksTodayYaml.unlocked) > 0:
                click.secho(f"Unlocked tasks:")
                for task in tasksTodayYaml.unlocked:
                    click.secho(f" ({taskI}) " + task.text(brackets=True))
                    taskI += 1
                click.secho()
        
        with ConfigYaml() as configYaml:
            graceSec = tasksTodayYaml.getGraceTimeSeconds(configYaml.dayEndsAt)
            if graceSec is not None:
                graceHrs = graceSec // 3600
                graceSec %= 3600
                graceMin = graceSec // 60
                graceSec %= 60
                click.secho(f"Grace time: {graceHrs}:{str(graceMin).zfill(2)}:{str(graceSec).zfill(2)}")
                click.secho()
        
        if len(tasksTodayYaml.finished) > 0:
            click.secho(f"Finished tasks:")
            for task in tasksTodayYaml.finished:
                click.secho("  -  " + task.text(brackets=True))
            click.secho()


@cli.command()
@click.argument("index", type=int, default=-1)
def cancel(index):
    """Removes the task with a specific index from today's tasks"""
    if index < 0:
        index = click.prompt("Index of task to remove", type=int)
    if index < 0:
        cliError(f"Index must not be negative")
        return
    with TasksTodayYaml() as tasksTodayYaml:
        if index >= len(tasksTodayYaml.startableTasks):
            cliError(f"Index too high (there are only {len(tasksTodayYaml.startableTasks)} tasks)")
            return
        task = tasksTodayYaml.startableTasks[index]
        tasksTodayYaml.removeTask(task)
        click.secho(f"Removed task {task.text(brackets=True)}")

@cli.command()
@click.argument("minutes", type=int, default=-1)
@click.argument("activity", default="")
@click.option("-o", "--one-time-activity", "oneTimeActivity", is_flag=True, help="If the specified activity doesn't exist, creates an activity without saving it to activities.yaml")
@click.option("-s", "--scheduled-at", "scheduledAtStr", default="", help="Time that the task is scheduled at (currently purely visual)")
@click.option("-b", "--before", "beforeStr", default="", help="Time that the task must be completed before (currently purely visual)")
@click.option("-a", "--after", "afterStr", default="", help="Time that the task must be completed after (currently purely visual)")
@click.option("-br", "--breaks", "breaksStr", default="", help="List of mid-task breaks as pairs of timestamps and durations, formatted as '30 5 60 5' etc")
@click.option("-u", "--unlocks", "unlocks", type=(int, str), default=None, help="Task to be unlocked, formatted as 'durationMinutes activityName'")
def add(minutes: int, activity: str, oneTimeActivity: bool, scheduledAtStr: str, beforeStr: str, afterStr: str, breaksStr: str, unlocks: Tuple[int, str] | None):
    """Schedules a new task for today"""
    if minutes < 0:
        minutes = click.prompt("Task duration (in minutes)")
    if minutes < 0:
        cliError("Duration minutes can't be negative.")
        return
    if activity == "":
        activity = click.prompt("Name of task activity")
    
    scheduledAt = activities_tasks.parseTime(scheduledAtStr) if scheduledAtStr else None
    before = activities_tasks.parseTime(beforeStr) if beforeStr else None
    after = activities_tasks.parseTime(afterStr) if afterStr else None
    
    with ActivitiesYaml() as activitiesYaml:
        activityObj = activities_tasks.find(lambda a: a.name == activity, activitiesYaml.allActivities)
        if activityObj is not None and oneTimeActivity:
            cliError(f"Activity '{activity}' already exists, cannot create one-time activity")
        if not activityObj:
            if not oneTimeActivity:
                oneTimeActivity = click.confirm(f"Activity '{activity}' not found. Create a one-time activity for this task?", abort=True)
            activityObj = activities_tasks.Activity(name=activity)
        
        unlockTask = None
        if unlocks is not None:
            unlockMin = unlocks[0]
            if unlockMin < 0:
                cliError("Unlockable task's duration minutes can't be negative.")
                return

            unlockActivityObj = activities_tasks.find(lambda a: a.name == unlocks[1], activitiesYaml.allActivities)
            if not unlockActivityObj:
                if not oneTimeActivity:
                    cliError(f"Activity '{unlocks[1]}' for the unlockable task not found.")
                    return
                unlockActivityObj = activities_tasks.Activity(name=activity)
            
            unlockTask = activities_tasks.Task(
                activity=unlockActivityObj,
                durationMin=unlockMin,
            )
        
        breaks = []
        if breaksStr:
            breaksIter = iter(breaksStr.split(" "))
            for timestampStr in breaksIter:
                durationStr = next(breaksIter)
                timestamp, duration = int(timestampStr), int(durationStr)
                breaks.append(activities_tasks.Break(durationMin=duration, timestampMin=timestamp))

        
        task = activities_tasks.Task(
            durationMin=minutes,
            activity=activityObj,
            scheduledAt=scheduledAt,
            before=before,
            after=after,
            breaks=breaks,
            unlocks=unlockTask,
        )
        
        with TasksTodayYaml() as tasksTodayYaml:
            tasksTodayYaml.tasks.append(task)
            i = tasksTodayYaml.startableTasks.index(task)
            click.secho(f"Successfully added task at index {i}:")
            click.secho(" - " + task.text(brackets=True))


@cli.command()
@click.argument("index", type=int, default=-1)
@click.option("-c", "--choice", "choice", default="", help="If the task is a choice activity, specifies the name of the choice")
def start(index: int, choice: str):
    """Starts the task with a specific index"""
    if index < 0:
        index = click.prompt("Index of task to start", type=int)
    if index < 0:
        cliError(f"Index must not be negative")
        return
    with TasksTodayYaml() as tasksTodayYaml:
        if index >= len(tasksTodayYaml.startableTasks):
            cliError(f"Index too high (there are only {len(tasksTodayYaml.startableTasks)} tasks)")
            return
        task = tasksTodayYaml.startableTasks[index]

        if isinstance(task.activity, activities_tasks.ChoiceActivity):
            if choice:
                activity = activities_tasks.find(lambda act: act.name == choice, task.activity.choices)
                if activity is None:
                    cliError(f"Choice activity '{task.activity.name}' does not contain choice '{choice}'")
                    return
                else:
                    task.chosenFrom = task.activity
                    task.activity = activity
            else:
                choices = []
                for activity in task.activity.choices:
                    choices.append((
                        activity,
                        activity.name,
                    ))
                chosenActivity = ui.choice(choices, title=f"Select a task out of '{task.activity.name}':")
                task.chosenFrom = task.activity
                task.activity = chosenActivity

        if task in tasksTodayYaml.unlocked:
            task.unlocked = True
        tasksTodayYaml.removeTask(task)
        task.startedAt = datetime.now().time()
        tasksTodayYaml.current = task
        click.secho(f"Started task the following task!")
        click.secho(" + " + tasksTodayYaml.current.text(brackets=True))

@cli.command()
def stop():
    """Cancels the currently active task"""
    with TasksTodayYaml() as tasksTodayYaml:
        if not tasksTodayYaml.current:
            cliError(f"No task currently active")
            return
        current = tasksTodayYaml.current
        if current.unlocked:
            tasksTodayYaml.unlocked.insert(0, current)
        else:
            tasksTodayYaml.tasks.insert(0, current)
        tasksTodayYaml.current = None

        current.unlocked = None
        if current.startedAt:
            current.finished = current.finishedNow
            current.startedAt = None

        i = tasksTodayYaml.startableTasks.index(current)
        click.secho(f"Stopped the current task. (it is now at index {i})")
        click.secho(" + " + current.text(brackets=True))


@cli.command()
@click.option("-c", "--confirm", "confirm", is_flag=True, help="Skips the confirmation prompt")
def generate(confirm: bool = False):
    """Re-generates the task list for today"""
    with TasksTodayYaml() as tasksTodayYaml:
        if not confirm and len(tasksTodayYaml.allTasks) > 0:
            click.confirm(f"Confirm re-generate tasks? (the existing {len(tasksTodayYaml.allTasks)} will be erased)", abort=True)
        tasksTodayYaml.regenerateTasks(force=False)


@cli.command()
@click.argument("indices", type=int, nargs=-1)
def combine(indices: List[int]):
    """Combines two tasks with the same activity"""
    for i in indices:
        if i < 0:
            cliError(f"Index {i} may not be negative")
            return
    with TasksTodayYaml() as tasksTodayYaml:
        taskCount = len(tasksTodayYaml.startableTasks)
        indicesTooHigh = []
        for i in indices:
            if i > taskCount:
                indicesTooHigh.append(i)
        if len(indicesTooHigh) > 0:
            if len(indicesTooHigh) == 1:
                pre = "Index"
            else:
                pre = "Indices"
            cliError(f"{pre} {', '.join(indicesTooHigh)} too high (there are only {len(tasksTodayYaml.startableTasks)} tasks)")
            return
        tasks = []
        for i in indices:
            tasks.append(tasksTodayYaml.startableTasks[i])
        activity = tasks[0].activity
        for i, task in enumerate(tasks):
            if task.activity.name != activity.name:
                cliError(f"Tasks must have the same activity to be combined. Task {i} is '{task.activity.name}', not '{activity.name}'")
                return
        finishedSec = tasks[0].durationMin * 60 * tasks[0].finished
        for task in tasks[1:]:
            tasks[0].durationMin += task.durationMin
            finishedSec += task.durationMin * 60 * task.finished
            tasksTodayYaml.removeTask(task)
        if finishedSec > 0:
            tasks[0].finished = finishedSec / (tasks[0].durationMin * 60)
        click.secho(f"Combined {len(indices)} tasks.")
