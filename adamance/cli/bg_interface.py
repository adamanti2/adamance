import os
import datetime
import subprocess
from pathlib import Path

from adamance.shared.consts import BG_PATH

def isBgRunning():
    lockPath = BG_PATH / "bg.lock"
    if not os.path.isfile(lockPath):
        return False
    mtime = os.path.getmtime(lockPath)
    currentTime = datetime.datetime.now().timestamp()
    if currentTime - mtime < 1:
        return True
    return False

def stopBg():
    stopPath = BG_PATH / "stop.lock"
    with open(stopPath, "w") as f:
        f.write("")

def startBg():
    subprocess.Popen(["python3", "-m", "adamance.background"])
