import click
from typing import Any, Iterable, Tuple, Optional, TypeVar

T = TypeVar("T")

def choice(choices: Iterable[Tuple[T, str]], title: Optional[str] = None, prefix: Optional[str] = None) -> T:
    if prefix is None:
        prefix = ""
    if title is not None:
        click.secho(prefix + title)
    
    keyIndices = {}
    for i, choice in enumerate(choices):
        key, label = choice
        keyIndices[i] = key
        click.secho(prefix + f"  {i}. " + label)
    limit = len(keyIndices.keys()) - 1
    
    while True:
        selectedI = click.prompt(prefix + "Selected option", type=int)
        if selectedI < 0 or selectedI > limit:
            click.secho(prefix + f"Error: index must be between 0 and {limit}.")
        else:
            break
    
    return keyIndices[selectedI]

