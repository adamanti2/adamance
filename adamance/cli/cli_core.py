from .bg_interface import isBgRunning
from adamance.shared import consts
import click

def cliError(text: str):
    click.secho(f"Error: {text}", fg="red")

class AdamanceGroup(click.Group):
    def format_help(self, ctx, formatter):
        click.secho(f"----- adamance {consts.__version__} -----\n")
        super().format_help(ctx, formatter)
        if not isBgRunning():
            formatter.write(click.style("\nWarning: adamance's background is not running. Start it with 'adamance bg start'", fg="yellow"))

@click.group(cls=AdamanceGroup)
def cli():
    pass

_dontCheckBg = False

@cli.result_callback()
def checkBg(*_args, **_kwargs):
    global _dontCheckBg
    if not _dontCheckBg and not isBgRunning():
        click.secho("Warning: adamance's background is not running. Start it with 'adamance bg start'", fg="yellow")
    if _dontCheckBg:
        _dontCheckBg = False

def disableBgCheck():
    global _dontCheckBg
    _dontCheckBg = True
