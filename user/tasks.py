from adamance.shared.activities_tasks import Task, Activity, find
from typing import List

from datetime import datetime, time

def __tasks__(activities: List[Activity]) -> List[Task]:
    # The __tasks__ function returns a list of tasks to generate when a new day starts.
    # This function's only parameter accepts the list of all activities specified in activities.yaml.
    return []
